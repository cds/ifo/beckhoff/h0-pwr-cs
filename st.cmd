# This file requires a blank line at the end.

dbLoadDatabase("./tCat.dbd",0,0)
tCat_registerRecordDeviceDriver(pdbbase)
callbackSetQueueSize(10000)

tcSetScanRate(10, 5)
tcGenerateList ("C:\SlowControls\TwinCAT3\Sandbox\Target\H0PWRCS\h0pwrcs.txt", "-rv -l -ps")
tcGenerateList ("C:\SlowControls\TwinCAT3\Sandbox\Target\H0PWRCS\h0pwrcs.req", "-rv -lb -ps")
tcGenerateList ("C:\SlowControls\TwinCAT3\Sandbox\Target\H0PWRCS\h0pwrcs.ini", "-rv -l -ns -ps")
tcLoadRecords ("C:\SlowControls\TwinCAT3\Sandbox\Target\H0PWRCS\PLC1\PLC1.tpy", "-rv")

iocInit()
